create table customer (
    id    varchar(36),
    nama  varchar(255) not null,
    email varchar(100) not null,
    no_hp varchar(50)  not null,
    alamat varchar(255),
    foto varchar(100),
    primary key (id),
    unique (email),
    unique (no_hp)
);
