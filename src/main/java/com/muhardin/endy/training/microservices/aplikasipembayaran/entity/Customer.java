package com.muhardin.endy.training.microservices.aplikasipembayaran.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity @Data
public class Customer {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty @Size(min = 3, max = 255)
    private String nama;

    @Email @NotNull @NotEmpty @Size(min = 3, max = 100)
    private String email;
    @NotNull @NotEmpty @Size(min = 3, max = 50)
    private String noHp;

    @Size(max = 255)
    private String alamat;
    @Size(max = 100)
    private String foto;
}
